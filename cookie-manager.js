class CookieManager {
  constructor(name, data) {
    this.name = name;
    this.data = data;
  }

  setCookie() {
    let d = new Date(Date.now() + 900000);

    document.cookie =
      this.name + "=" + this.data + ";" + "expires=" + d.toUTCString();
  }

  getCookie() {
    const name = this.name + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(";");
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  clearCookie() {
    return (document.cookie = this.name + "=" + this.data + ";" + "max-age=0");
  }
}

let x = new CookieManager("Robert", "Data-dadad-gsdgdft-534gethtr");
let y = new CookieManager("kebab", "sdhfgsdjhfgsdjhgfsdjhgfj");
// x.setCookie();
// y.setCookie();
// x.getCookie();
// x.clearCookie();
